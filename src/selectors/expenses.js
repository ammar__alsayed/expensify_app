import moment from 'moment';

//get visive expenses
export default (expenses, { sortBy, startDate,endDate,text } = filters) => {
    
    // get the expenses that matches filtering options only
   return expenses.filter( (expense) => {
       //   cases for filtring: 
    
       // DISPLAY ONLY THE EXPENSES CREATED BETWEEN  START DATE AND AND DATE if specified
       const createdAtMoment = moment(expense.createdAt); // change it to moment
       // if no startDate, return true always, no filtering
       // if the is start date, check if  the createdAt day is before or same start day 
       const startDateMatch = startDate ? startDate.isSameOrBefore(createdAtMoment,'day'):true;
       const  endDateMatch = endDate ? endDate.isSameOrAfter(createdAtMoment,'day'):true;
        // search for the word in lowercased description
       const TextMatch = expense.description.toLowerCase().includes(text.toLowerCase());
        // if one is false the expense will bot be shown
       return startDateMatch && endDateMatch && TextMatch;  
   }).sort( (a , b) => {
       if(sortBy === 'date'){
           return a.createdAt < b.createdAt ? 1: -1; 
       }else if(sortBy === 'amount'){
          return a.amount < b.amount ? 1: -1;
       }
   });

};