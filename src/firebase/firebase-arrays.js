// take all named exports and put them in variable firebase 
import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database'; // If using Firebase database

// Initialize Firebase
const config = {
    apiKey: "AIzaSyB9HnPgTP11KmvrzjreNDXdPLh5I7HqaHs",
    authDomain: "expensify-8b3fd.firebaseapp.com",
    databaseURL: "https://expensify-8b3fd.firebaseio.com",
    projectId: "expensify-8b3fd",
    storageBucket: "expensify-8b3fd.appspot.com",
    messagingSenderId: "595624472444"
  };
  firebase.initializeApp(config);
  const db = firebase.database();


  /*  when working with arrays, we use push that will generate a random 
   id for each index
  
  db.ref('persons').push({
      name: 'Sami',
      age: 30,
  })  */
 // update info
  db.ref('persons/-LJh4et_xJqocQEbAnVs').update({
    age: 40
  })
  db.ref("expenses").push({
      description: 'Gas bill',
      amount: 3043,
      createdAt: 152343400000
  })
  
  db.ref().on('value', (snapshot) =>{
    console.log(snapshot.val())
}, (e)=>{ // catch
    console.log('error fetching data:' ,e)
})