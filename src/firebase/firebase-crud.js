// take all named exports and put them in variable firebase 
import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database'; // If using Firebase database

// Initialize Firebase
const config = {
    apiKey: "AIzaSyB9HnPgTP11KmvrzjreNDXdPLh5I7HqaHs",
    authDomain: "expensify-8b3fd.firebaseapp.com",
    databaseURL: "https://expensify-8b3fd.firebaseio.com",
    projectId: "expensify-8b3fd",
    storageBucket: "expensify-8b3fd.appspot.com",
    messagingSenderId: "595624472444"
  };
  firebase.initializeApp(config);
  const database = firebase.database();

    //   write data into the db

  database.ref().set({ // the info will be in the root of the db
      name: 'Ammar',
      age: 24,
      location:{
          country: 'Yemen',
          city: 'Mukalla'
      }
  }) 
//database.ref().set('override the data ');

database.ref('location/city').set('Aden')
// add new attribute
database.ref('size').set({
height: '167 cm',
weight: '68 kg'
}).then(() =>{
    console.log('data is saved!');
}).catch((e)=>{
    console.log('error in', e);
})


/* remove data from db

database.ref('size/hieght').remove()
.then(()=>{
    console.log('data removed');
}).catch((e)=>{
    console.log('data not removed', e)
})

*/

/* update an existing value 

database.ref().update({
name: 'Ali',
age: 40,
job: {
    title:'web developer',
    company: 'ClickApps'
    }, // add job
'size/weight': null  // delete wieght
}); 
 
*/

/* fetching data from db */

    // fetch all the data at once

/* database.ref().once('value')  // require promises
.then((snapshot)=>{
  const dataFetched = snapshot.val()  
console.log('data is', dataFetched) // print the data
}).catch((e)=>{
    console.log('error fetching data,', e)
}) */

/*   fetch  data (best way)  */
const onValueChange =  database.ref().on('value', (snapshot) =>{
    console.log(snapshot.val())
}, (e)=>{ // catch
    console.log('error fetching data:' ,e)
})
database.ref().on('value', (snapshot)=>{
    const val = snapshot.val();
    console.log(` ${val.name} is ${val.job.title} at ${val.job.company} `)
})

setTimeout(() => {
    database.ref('age').set(25)
}, 3500);

// unsebscribe 
setTimeout(() => {
    database.ref().off('value',onValueChange);
    console.log('unsubscribe')
}, 7000);

// data will not print the console coz of unsubscription
setTimeout(() => {
    database.ref('age').set(26)
}, 10000);