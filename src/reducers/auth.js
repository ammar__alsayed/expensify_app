export default (state={}, action)=>{
    switch(action.type){
        case 'LOGIN':
        return {
            uid: action.uid // return user id
        };
        case 'LOGOUT':
        return {}; // return empty state
        default: 
        return state;
    }
}