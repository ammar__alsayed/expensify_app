import expenses from "../selectors/expenses";

// set up default expense state
 const expensesReducerDefaultState = [];
// expense reducer
export default (state = expensesReducerDefaultState  , action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
        return [...state, action.expense];

        case 'REMOVE_EXPENSE':
        return state.filter( (expense) => ( //object destructuring
            expense.id !== action.id
        ));

        case 'EDIT_EXPENSE' :
        return state.map( (expense ) => {
            if( expense.id === action.id ){
              return{ 
                    ...expense, // pass exisiting state
                    ...action.updates // override only passed updates
                };
            }else{
                return expense; // no changes
            }
        });
        case 'DISPLAY_EXPENSES' :
        return action.expenses;
        default:
            return state;
    }
};
