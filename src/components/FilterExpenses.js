import React from 'react';
import {connect} from 'react-redux';
import 'react-dates/initialize';
import {DateRangePicker} from 'react-dates';
import { setTextFilter, sortByAmount,sortByDate,setStartDate,setEndDate } from "../actions/filters";

class FilterExpenses extends React.Component{
    state = {
        calendarFocused: null
    }
    onDatesChange = ({startDate,endDate}) => {
        this.props.dispatch(setStartDate(startDate));
        this.props.dispatch(setEndDate(endDate));
    }
    onFocusChange = (calendarFocused)=>{
        this.setState({calendarFocused})
    }
    render(){
        return (
            <div className='content-container'> 
            <div className='input-group'>
                <div className='input-group__item'>
                <input 
                className='text-input'
                type='text' 
                placeholder='Search Expense'
                value= {this.props.filters.text} // get input from user
                onChange= { (e) => {
                    // dispatch user input to the state
                    this.props.dispatch(setTextFilter(e.target.value))
                 }
                  } 
                /> 
                </div>
                <div className='input-group__item'>
                <select 
                className='select'
                value = {this.props.filters.sortBy || 'date' }// get the value from user
                 onChange = { (e)=>{
                  if (e.target.value === 'date') {
                    this.props.dispatch(sortByDate());
                  }else if(e.target.value === 'amount'){
                    this.props.dispatch(sortByAmount());
                  }
        
                }}>
                 <option value = 'date'> Date </option>
                 <option value = 'amount' > Amount </option>
                </select>
                </div>
                <div className='input-group__item'>
                <DateRangePicker
                    startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                    startDate={this.props.filters.startDate} // momentPropTypes.momentObj or null,
                    endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                    endDate={this.props.filters.endDate} // momentPropTypes.momentObj or null,
                    onDatesChange={this.onDatesChange} // PropTypes.func.isRequired,
                    focusedInput={this.state.calendarFocused} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                    onFocusChange={this.onFocusChange} // PropTypes.func.isRequired,
                    numberOfMonths={1}
                    showClearDates = {true}
                    isOutsideRange={() => false }

                />
                </div>
              </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
 return{
    filters : state.filters 
 };
};

export default connect(mapStateToProps)(FilterExpenses) ;