import React from 'react';
import {connect} from 'react-redux'; // connect component to redux store
import ExpenseListItem from './ExpenseListItem';
import SelectExpenses from '../selectors/expenses';

// unconnected component
const ExpenseList = (props) => (
    <div  className='content-container'>
        <div className='list-header' >
            <div className='show-on-mobile' > Expenses </div>
            <div className='show-on-desktop'> Expense </div>
            <div className='show-on-desktop'> Amount </div>
        </div>
        <div className='list-body' >
            { 
                props.expenses.length === 0 ? (
                    <div className='list-item list-item--msg' >
                        <span> No Expneses </span> 
                    </div>
                ):(
                    props.expenses.map((expense ,index ) => (

                    <ExpenseListItem  // print it

                    key ={index+1}
                        {...expense} // spread out all properties in the expense i.e: id, amount ...etc.
                    />
                    ))
                ) 
            }
        
        </div>
        
         
   
    </div>
);
 
// pass state info to props to render it
const mapStateToProps = (state) => {
    return {
        // apply filtering options 
        expenses: SelectExpenses(state.expenses, state.filters)
    }
};

// the connected component 
export default connect(mapStateToProps)(ExpenseList);


