 import React from 'react';
import moment from 'moment';
import 'react-dates/initialize';
import { SingleDatePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

// working with dates
const date = moment().format("MMM DD, YYYY");
 export default class ExpenseFom extends React.Component{
     constructor(props) {
         super(props); 
         // state for add/edit state
         this.state = {
            description: props.expense? props.expense.description : '',
            amount: props.expense? props.expense.amount.toString() :'',
            note: props.expense? props.expense.note :'',
            createdAt: props.expense? moment(props.expense.createdAt) :moment(), // create new moment
            calendarFocused: false,
            error: ''
            }
     }
     
     
     onDescriptionChange= (e)=>{
        const description =  e.target.value;
        this.setState( () => ({ // set the new description
            description  // short of description: description
        }));
     };
     onAmountChange= (e)=>{
         const amount = e.target.value;
         // enforce the input to be only numbers with only two decemal digits
         if(amount.match(/^\d*(\.\d{0,2})?$/)){
            this.setState( () => ({
                amount
            }));
         }};
     onNoteChange=(e)=>{
        this.setState({ 
            note: e.target.value 
        });
     };
     onDateChange =  (createdAt) =>{
         if(createdAt){
        this.setState({ createdAt })
         }
     };
     onFocusChange = ( {focused} ) => { // put it in { } coz it is boolean
         this.setState({ calendarFocused:focused })
     };

     onSubmit = (e) => {
       e.preventDefault(); // prevent page loading
       // if user dont ennter desc or amount
       if(!this.state.description || !this.state.amount){
           //throw error
            this.setState({ error: 'please enter description and amount' });
       }else{
           // clear the error
        this.setState({error: ''});
        // submit the state up for dispatching 
        this.props.onSubmit({
            description: this.state.description,
            amount: parseFloat(this.state.amount), // change from string to float
            createdAt: this.state.createdAt.valueOf(), // change it from moment object to date
            note: this.state.note
        })
        }
     }

    render() {
        return (
            
                <form className='form' onSubmit = { this.onSubmit }>
                     {this.state.error && <p className='form__error'> {this.state.error} </p> }
                    <input 
                    className='text-input'
                    type='text' 
                    name='description' 
                    placeholder='Description' 
                    autoFocus
                    value = {this.state.description} // get the value from the state
                    onChange={this.onDescriptionChange} 
                    />
                    <input 
                    className='text-input' 
                    type='text' 
                    name='amount' 
                    placeholder='Amount' 
                    autoFocus
                    value = {this.state.amount}
                    onChange = {this.onAmountChange}
                    />
                    <SingleDatePicker
                        date={this.state.createdAt}
                        onDateChange={this.onDateChange} 
                        focused={this.state.calendarFocused} 
                        onFocusChange={this.onFocusChange} 
                        numberOfMonths= {1}
                        isOutsideRange = {()=> false}
                        />
                    <textarea  
                    className='textarea'
                    autoFocus
                    value = {this.state.note}
                    onChange = {this.onNoteChange}
                    placeholder= 'Enter a note for your expense (optional)'
                    >
                    </textarea>
                    <div>
                    <button className='button' value='submit'> Save Expense </button>
                     </div>
                </form>
            
        );
    }

 }
