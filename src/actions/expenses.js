import database from '../firebase/firebase';

  // expenses actions
 // - ADD_EXPENSE
 export const addExpense = (expense) => ({
    type: 'ADD_EXPENSE',
    expense
    });

   // adding using redux-thunk
  export  const startAddExpense = (expenseData = {}) => {
      // return a function (thunk)
    return (dispatch, getState) => {
        const userId = getState().auth.uid;
        // set up default values
        const {
            description = '' , 
            note = '',
            amount = 0 , 
            createdAt = 0 
        } = expenseData;

        const expense = {description, amount,createdAt,note};
        return database.ref(`users/${userId}/expenses`).push(
            expense
        ).then( (ref)=>{
            dispatch(addExpense({
                id: ref.key,  // get the id from ref
                ...expense
            }))
        })
    }
    }
// - REMOVE_EXPENSE
export const removeExpense =  (id) => ({
   type: 'REMOVE_EXPENSE',
   id
});
export const startRemoveExpense = (id) => {
    return (dispatch, getState) => {
        const userId = getState().auth.uid;
        // remove from firebase then dispatch (remove from state)
        return database.ref(`users/${userId}/expenses/${id}`).remove().then( ()=>{

             dispatch(removeExpense(id));
         })
    }
}


// - EDIT_EXPENSE
export const editExpense =  (id, updates) => ({
   type:'EDIT_EXPENSE',
   id,
   updates
});

export const startEditExpense = (id, updates)=>{
    return (dispatch, getState) => {
        const userId = getState().auth.uid;
        return database.ref(`users/${userId}/expenses/${id}`).update( // update on firebase
            updates
        ).then(()=>{ // remove from state
            dispatch(editExpense(id, updates))
        })
    }
}

// - SET_EXPENSES
export const displayExpenses =  (expenses) => ({
    type:'DISPLAY_EXPENSES',
    expenses
 });
 
 export const startDisplayExpenses = () =>{
    return (dispatch, getState) => {
       const userId = getState().auth.uid;
       return database.ref(`users/${userId}/expenses`).once('value').then((snapshot) => {
            const expenses = [];
            snapshot.forEach((childSnapshot) => {
                expenses.push({ // push it to the array
                id: childSnapshot.key,
                ...childSnapshot.val()
                }); 
            });
            dispatch(displayExpenses(expenses))
        });
    }
 }