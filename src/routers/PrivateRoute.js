import React from 'react';
import { connect } from 'react-redux';
import {Route, Redirect} from 'react-router-dom';
import Header from '../components/Header';


 export const PrivateRoute = ({
     isAuthenticated,
     component: Component,
     ...rest  // variable contain all props that are not desctructered
    }) => (
    <Route {...rest} component = { (props) => (
        isAuthenticated ? (
            <div>
            <Header />
            <Component {...props} />
            </div>
        ) :( 
            <Redirect to='/' />
        )
    )} />
 );

 const mapPropsToState = (state) => ({
    isAuthenticated: !!state.auth.uid // get the boolean value
 }); 
 export default connect(mapPropsToState)(PrivateRoute)