const person = {
    //name: 'Ammar',
    age: 30,
    location: {
        country: "Yemen",
        city: 'Hadramout'
    },
};

// console.log(` ${Person.name} is ${Person.age}. `);

/* applying destructuring 
  const name = person.name;
  const age = person.age;

  or the short handed way
 */

 // renaming and setting default value 
const {name : firstname= 'Anonymus' , age} = person;
const {country: region, city} = person.location;

console.log(` ${firstname} is ${age} years old,
 and he is from ${city}, ${region}  `);
