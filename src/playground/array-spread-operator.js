/** 
  Spread operators - allowes you to add add items to an existing array,
  its equivilant to array.concat.

  it make new array with the added items but doesn't affect the original
  array

 */

 const names = ['ali', 'ahmed'];

 console.log(names);
 // using spread operators
 console.log([...names]);

 // create new array 
 console.log([...names, "Salem"]);
 console.log(['Ammar', ...names, 'salah']);
 

 // the original array not changed
 console.log(names);
 