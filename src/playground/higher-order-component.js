 import React from 'react';
 import ReactDOM from 'react-dom';
 
 /**  Higher Order Component (HOC) is a component that render another component 
  Advantages of HOC:
  - reuse code
  - render hijacking
  - prop manipulation
  - abstract state
 */

 // normal stateless component
 const TestResult = (props) =>( 
     <div>
        <h1> Test Result </h1>
        <p> the result is:   </p>
     </div>
);
const requireAuthentication= (Component1) => {
    return (props) => (
        <div>
        <Component1 {...props} />
        {props.isAuthenticated ?  props.info : <p> you are not authorized to see this message  </p>}        
        </div>
    )
};

// HOC
const adminWarningMsg =(AnotherComponent)=>{
    return (props) => (
        // ...props take everything in the props
        <div> 
            { props.isAdmin && <p> This message is confidential, please don't share! </p>}
            <AnotherComponent {...props}  />
        </div>
       
    );
};

    //const WarnAdmin =  adminWarningMsg(TestResult); // pass userInfo to it
 //ReactDOM.render(<WarnAdmin isAdmin={true} info = 'you are healthy' />, document.getElementById('app'));

const AuthInfo = requireAuthentication(TestResult)
 ReactDOM.render(<AuthInfo isAuthenticated={true} info = 'you are healthy' />, document.getElementById('app'));
