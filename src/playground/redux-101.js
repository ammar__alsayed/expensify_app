import {createStore} from 'redux';


// if object passed use disctructuring
// if object passed with no value use 1 as default
// if no object passed use {} empty object
const incCount = ( {incBy = 1} = {} ) => ({
    type: 'INCREAMENT',
    // incBy: incBy  shorted
    incBy
});
const decCount = ( {decBy = 1} = {} ) => ({
    type: 'DECREMENT',
    decBy
});
const setCount = ( {count} ) => ({
    type: 'SET',
    count
});
const reset = () => ({
    type: "RESET",
    count: 0
});

/**     Reducer
    1. Reducer are pure functions (the output totally depends on the input)
    2. Never change state or action directly ( only return an object
    that represent a new state)
 */
  // set up default state in the reducer
  
const countReducer = ( state = {count: 0} , action )=>{
    switch (action.type){
        case 'INCREAMENT': 
        return{
            count:  state.count + action.incBy };
    
        case 'DECREMENT': 
        return{
                count:  state.count - action.decBy };
        case 'SET':
        return {count: action.count};   
        case "RESET": return { count:0 };
    
        default: return state;
     }
};
var say_hello = {state};
const store = createStore(countReducer);


// called whenever the state changed
 store.subscribe( () =>{
    console.log(store.getState());
 });

 store.dispatch(incCount());
 store.dispatch(incCount({incBy: 40}))
 store.dispatch(reset());
 store.dispatch(decCount({decBy:20}));
 store.dispatch(decCount());
 store.dispatch(setCount({count:233}));

