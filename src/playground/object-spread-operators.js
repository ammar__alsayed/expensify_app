const person = {
    name: 'ali',
    age: 30
}

/**  
 * object operators to change the object
 
   */
console.log(person);
console.log(
   //using spread operator
    {...person}
);
 // add new properties to the existing one
 console.log(
     {
         ...person,
        location: 'Yemen'
    }
 );

 // override existing properties
 console.log({
     ...person,
     age: 25,
     name: "Ammar",
     email: 'ammaralsayed2@gmail.com'
 });
 // another way
const child ={
    age: 12
}
 console.log({
     ...person,
     ...child
 });
 
 