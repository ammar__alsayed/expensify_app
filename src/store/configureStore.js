import { combineReducers, createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import expensesReducer from '../reducers/expenses';
import filterReducer from '../reducers/filters';
import AuthReducer from '../reducers/auth';

        // for redux dev-tool in the browser
const composeEnhancers = +  window.__REDUX_DEVTOOLS_EXTENSION__ || compose;

// store creation
export default  () =>{
    const store = createStore(
        // combine multiple reducers, it takes abject of (key, value ) of
        // root state name and reduce that manage the state 
        combineReducers({
            expenses: expensesReducer,
            filters: filterReducer,
            auth: AuthReducer
        }),
        /* 
    Redux Thunk middleware allows you to write action creators that return a function
    instead of an action. The thunk can be used to delay the dispatch of an action, 
    or to dispatch only if a certain condition is met. The inner function receives the
    store methods dispatch and getState as parameters. */
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
};


