const path = require('path');
const ExtractTextPluging = require('extract-text-webpack-plugin');

module.exports = (env) => {
  const isProduction = env === 'production';
  const CSSExtract = new  ExtractTextPluging('style.css')
  return{
    entry: ['babel-pollyfill','./src/app.js'],
    //entry: './src/playground/higher-order-component.js',
    output: {
      path: path.join(__dirname, 'public' ),
      filename: 'bundle.js',
      publicPath: '/'
    },
    module: {
      rules: [
        {
          loader: "babel-loader",
          // targeted files (.js files)
          test: /\.js$/,
          exclude: /node_modules/
          
        },{
          // targeted files (.scss and css files)
          test: /\.s?css$/,
          use: CSSExtract.extract({
            use:[
             {
              loader:'css-loader', 
              options:{
                sourceMap: true
              }
            },
            {
              loader:'sass-loader',
              options:{
                sourceMap: true
              }
              
            }
          ]
          })
        }] 
    },
    
    plugins: [ 
      CSSExtract 
    ],
    // for error handling
    devtool: isProduction? 'source-map' : 'inline-source-map',
    devServer: {
      contentBase: path.join(__dirname, 'public'),
      historyApiFallback: true,
      port: 3000, 
      hot: false,
      host: '127.0.0.1'
    }
  };
  
    
}
